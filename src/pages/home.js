import React from 'react';
import { useState } from 'react';
import styled, {keyframes} from 'styled-components';
import { Link } from 'react-router-dom';
import jsonFileM from '../media/dataM.json';
import jsonFileF from '../media/dataF.json';

const dataMRaw = jsonFileM.clothes;
const dataFRaw = jsonFileF.clothes;

const Wrapper = styled.div`
  background: #000000;
  position: fixed;
  height: 100vh;
  width: 100vw;
  display: flex;
  flex-direction: column;
  align-items: center;
`

const Title = styled.h1`
  font-size: 7vmin;
  color: #EEEEEE;
  align: center;
  text-shadow: 1vmin 1vmin 1vmin #231E23;
  margin-right: 30vw;
`

const Subtitle = styled.h2`
  font-size: 5vmin;
  color: #EEEEEE;
  text-shadow: 1vmin 1vmin 1vmin #231E23;
`

const Navbar = styled.div`
  width: 100vw;
  height: 12vh;
  position: fixed;
  top: 0;
  background-color: #BD4B4B;
  display: flex;
  align-items: center;
  justify-content: space-evenly;
`

const Links = styled.nav`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  width: 40vw;
  background-color: #EFB7B7;
  font-size: 2.2vw;
  border-radius: 7vh;
  height: 8vh;
  text-shadow: .3vmin .3vmin .6vmin #231E23;
  box-shadow: 1vmin 1vmin 3vmin #231E23;
`

const Spacer = styled.div`
  position: block;
  height: 23vh;
`

const colorAnimation = keyframes`
  0% { background-color: #EFB7B7;}
  15% { background-color: #BD4B4B;}
  90% { background-color: #BD4B4B;}
  100% { background-color: #EFB7B7;}
`

const Clothes = styled.div`
  display: flex;
  flex-direction: columns;
  align-items: center;
  justify-content: space-evenly;
  height: 20vh;
  width: 100vw;
  height: 40vh;
  margin-bottom: 2vh;
  animation-name: ${colorAnimation};
  animation-duration: 4s;
  animation-iteration-count: infinite;
  animaton-timing-function: ease-in;
`

const Item = styled.div`
  margin: 5vmin;
  padding: 2vmin;
  background-color: #000000;
  color: white;
  border-radius: 4vmin;
  width: 12vw;
  display: flex;
  flex-direction: column;
  align-items: center;
  border: .2rem white solid;
  box-shadow: 1vmin 1vmin 3vmin #231E23;
`

const Image = styled.img`
  width: 60%;
  max-height: 20vh;
  border-radius: 2vmin;
`

const Button = styled.button`
  background: black;
  width: 4vw;
  height: 4vw;
  border-radius: 2vw;
  color: white;
  box-shadow: 1vmin 1vmin 3vmin #231E23;
  font-size: 3vmin;
  display: flex;
  align-items: center;
  justify-content: center;
`

const CartBtn = styled.button`
  background: black;
  font-size: 1.5rem;
  color: white;
  margin-top: 1rem;
  border: .2rem #BD4B4B solid;
  border-radius: 1.5vmin;
`

const CartItems = styled.div`
  display: flex;
  flex-direction: column;
  height: 70vh;
  flex-wrap: wrap;
  align-content: center;
  justify-content: flex-start;
  width: 100vw;
  border-top-left-radius: 3vw;
  border-top-right-radius: 3vw;
  background: #BD4B4B;
`

const Home = () => {
  const [genre, setGenre] = useState('female');
  const [scroll, setScroll] = useState(0);
  const [dataM, setDataM] = useState(dataMRaw.slice(0, 3));
  const [dataF, setDataF] = useState(dataFRaw.slice(0, 3));
  const [cart, setCart] = useState([]);
  const [link, setLink] = useState('');

  const changeScroll = (num, size) => {
    console.log('scroll',scroll)
    if( scroll+num < 0 || scroll+num > size ){
      console.log('MaxScroll reached!', scroll);
      return 0;
    }
    setScroll(scroll+num);
    setDataM(dataMRaw.slice(0+scroll+num,3+scroll+num));
    setDataF(dataFRaw.slice(0+scroll+num,3+scroll+num));
    console.log(scroll);
  }

  const addToCart = (item) => {
    const {title, price} = item;
    if(cart.map((i)=>{
      if(i.indexOf(title)===0){
        return 0;
      }
    }).indexOf(0) >= 0){
      alert("Already on cart!");
      return 0;
    }
    setCart([...cart, [title, price]]);
    let total = 0;
    const newCart = (cart.map((i)=>{
      total += parseInt(i[1]);
      return ("%0A" + i[0] + " $" + i[1]);
    }).toString()) + "%0A%0A%0ATotal: $" + total.toString();
    setLink('https://api.whatsapp.com/send?phone=541159266209&text='+ newCart );
  }

  return (
    <>
      <Wrapper>
        <Navbar>
          <Title>SH%T Store</Title>
          <Links>
            <Link style={{textDecoration: "none", color: "black"}} to='/'>Home</Link>
            <Link style={{textDecoration: "none", color: "black"}} onClick={()=>{setGenre('male');setScroll(0);setDataM(dataMRaw.slice(0,3));}}>Male</Link>
            <Link style={{textDecoration: "none", color: "black"}} onClick={()=>{setGenre('female');setScroll(0);setDataF(dataFRaw.slice(0,3));}}>Female</Link>
            {/* <a style={{textDecoration: "none", color: "black"}} href={link}>Cart</a>           */}
            <Link style={{textDecoration: "none", color: "black"}} onClick={()=>{setGenre('cart')}}>Cart</Link>
          </Links>
        </Navbar>
        <Spacer />
        <Subtitle>{genre.toUpperCase()}</Subtitle>
        {(genre === 'cart') ? (<CartItems>
          {cart.map((item, id)=>{
            return (
              <Item key={id} style={{width: "18vw", flexDirection: "row", justifyContent: "space-evenly"}}>
                <h2>{item[0]}</h2>
                <h2>${item[1]}</h2>
                <Button style={{width: "2vw", height: "2vw"}} onClick={()=>{setCart(cart.filter((item)=>{return (id !== cart.indexOf(item))}))}}>X</Button>
              </Item> 
            )
          })}
        </CartItems>) : 
          (genre === 'male') ? 
            (<Clothes key='M'>
              <Button onClick={()=>{changeScroll(-3, dataMRaw.length)}}>&#60;</Button>
              {dataM.map((item, index)=>{
                const {title, img, price} = item;
                return (
                  <Item key={index}>
                    <h2>{title}</h2>
                    <Image src={img} alt='Picture'/>
                    <CartBtn onClick={()=>{addToCart(item)}}>${price}</CartBtn>
                  </Item>
                )
              })}
              <Button onClick={()=>{changeScroll(+3, dataMRaw.length)}}>&#62;</Button>
            </Clothes>) :
            (<Clothes key='F'>
              <Button onClick={()=>{changeScroll(-3, dataFRaw.length)}}>&#60;</Button>
              {dataF.map((itemF, index)=>{
                const {title, img, price} = itemF;
                return (
                  <Item key={index}>
                    <h3>{title}</h3>
                    <Image src={img} alt='Picture'/>
                    <CartBtn onClick={()=>{addToCart(itemF)}}>${price}</CartBtn>
                  </Item>
                )
              })}
              <Button onClick={()=>{changeScroll(+3, dataFRaw.length)}}>&#62;</Button>
            </Clothes>)}
      </Wrapper>
    </>
  )
}

export default Home
