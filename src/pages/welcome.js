import React from 'react';
import styled, { keyframes } from 'styled-components';
import { Link } from 'react-router-dom';

const Wrapper = styled.div`
  background: black;
  height: 100vh;
  width: 100vw;
  display: flex;
  justify-content: center;
  align-items: center;
`

const rotation = keyframes`
  0% {transform: rotate(0deg)}
  100% {transform: rotate(360deg)}
`

const Cut = styled.div`
  background-color: black;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 40vmin;
  height: 40vmin;
  border-radius: 50%;
  z-index: 2;
  border: 1vmin dashed white;
  box-shadow: 0vmin 0vmin 10vmin black;
  animation-name: ${rotation};
  animation-duration: 8s;
  animation-iteration-count: infinite;
  animation-timing-function: linear;
  position: fixed;

`

const Title = styled.h1`
  font-size: 12vmin;
  color: #EDEDED;
  position: absolute;
  align: center;
  z-index: 3;
  text-shadow: .7vmin .7vmin 3vmin #231E23;
`

const Line = styled.div`
  position: fixed;
  top: -100vmin;
  transform: rotate(25deg);
  background: #DA0037;
  width: 2vmin;
  height: 200vmax;
  box-shadow: 1vmin 1vmin 3vmin #444444;
  opacity: 0.7;
`

const welcome = () => {
  const lines = [];
  for(let i=0; i<50; i++){
    lines.push(i+1);
  }
  return (
    <>
    <Wrapper>
      <Cut></Cut>
        <Title>
          <Link to='/home' style={{textDecoration: 'none', color: 'white'}}>
            SH&T
          </Link>
        </Title>
    </Wrapper>
    {lines.map((line, index)=>{
      return (
        <Line style={{left: index*10-50+'vmin'}}/>
      )
    })}
    </>
  )
}

export default welcome
